/*
 * Copyright (C) 2015 Martin Klapetek <mklapetek@kde.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

import QtQuick 2.0
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.extras 2.0 as PlasmaExtras

import org.kde.plasma.ktplegacyhelper 1.0

Item {

    Plasmoid.busy: ktpLegacyHelper.isChangingPresence
    Plasmoid.fullRepresentation: Plasmoid.compactRepresentation
    Plasmoid.compactRepresentation: PlasmaCore.IconItem {
        id: panelIconWidget

        source: ktpLegacyHelper.currentPresenceIconName

        MouseArea {
            anchors.fill: parent
            acceptedButtons: Qt.LeftButton | Qt.MiddleButton
            onClicked: {
                if (mouse.button == Qt.MiddleButton || mouse.button == Qt.LeftButton) {
                    ktpLegacyHelper.toggleContactList();
                }
            }
        }
    }

    KTpLegacyPresence {
        id: ktpLegacyHelper

        onRequestedPresenceChanged: {
            updateTooltip();
        }

        onCurrentPresenceChanged: {
            updateTooltip();
        }

        onIsChangingPresenceChanged: {
            updateTooltip();
        }

        onConnectionStatusChanged: {
            updateTooltip();
        }
    }

    function updateTooltip() {
        if (ktpLegacyHelper.isChangingPresence) {
            if (ktpLegacyHelper.currentPresenceName == "offline") {
                Plasmoid.toolTipSubText = i18nc("Means 'Connecting your IM accounts', it's in the applet tooltip", "Connecting...");
            } else {
                Plasmoid.toolTipSubText = i18nc("The arg is the presence name (as is in ktp-common-internals.po, eg. Changing Presence to Away..., it's in the applet tooltip",
                                                "Changing Presence to %1...", ktpLegacyHelper.requestedPresenceName);
            }
        } else {
            Plasmoid.toolTipSubText = ktpLegacyHelper.currentPresenceName;
        }
    }

    function action_openIMSettings() {
        ProcessRunner.openIMSettings();
    }

    function action_openContactList() {
        ktpLegacyHelper.toggleContactList();
    }

    Component.onCompleted: {
        var presencesList = ktpLegacyHelper.presencesList;
        for(var i = 0; i < presencesList.length; ++i) {
            var disp = presencesList[i].displayString;
            var actionName = i;
            plasmoid.setAction(actionName, disp, presencesList[i].iconName);
        }
        plasmoid.setActionSeparator("statuses");

        // application actions
        plasmoid.setAction("openIMSettings", i18n("Instant Messaging Settings..."), "configure");
        plasmoid.setAction("openContactList", i18n("Contact List..."), "telepathy-kde");
        plasmoid.setActionSeparator("applications");

        // These require ktp-common-internals, reimplementing the whole widgets
        // here would be a bit meh
        // plasmoid.setAction("addContact", i18n("Add New Contacts..."), "list-add-user");
        // plasmoid.setAction("joinChatRoom", i18n("Join Chat Room..."), "im-irc");
        // plasmoid.setActionSeparator("actions");
    }

    function actionTriggered(actionName) {
        var number = parseInt(actionName)
        if (number !== null) {
            ktpLegacyHelper.setPresenceByIndex(number);
        } else {
            console.log("Unknown action", actionName);
        }
    }

}
