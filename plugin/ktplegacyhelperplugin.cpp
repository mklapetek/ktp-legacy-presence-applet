/*
    Copyright (C) 2014  Martin Klapetek <mklapetek@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "ktplegacyhelperplugin.h"
#include "global-presence.h"
#include "presence.h"

#include <QtQml>
#include <QProcess>
#include <QUrl>

class ProcessRunner : public QObject
{
    Q_OBJECT
    public:
        Q_INVOKABLE void openIMSettings() const {
            QProcess::startDetached("kcmshell4", QStringList() << "kcm_ktp_accounts" << "kcm_ktp_integration_module");
        }
};

static QObject *processrunner_singleton_provider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    return new ProcessRunner();
}

void KtpLegacyHelperPlugin::registerTypes(const char *uri)
{
    Q_ASSERT(uri == QLatin1String("org.kde.plasma.ktplegacyhelper"));

    qmlRegisterType<KTpLegacy::GlobalPresence>(uri, 1, 0, "KTpLegacyPresence");
    qmlRegisterType<KTpLegacy::PresenceObject>(uri, 1, 0, "PresencesList");
    qmlRegisterSingletonType<ProcessRunner>(uri, 1, 0, "ProcessRunner", processrunner_singleton_provider);
}

#include "ktplegacyhelperplugin.moc"
