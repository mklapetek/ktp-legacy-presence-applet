# Translation of plasma-applet_org.kde.ktp-contactlist.po to Brazilian Portuguese
# Copyright (C) 2015 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# André Marcelo Alvarenga <alvarenga@kde.org>, 2015.
msgid ""
msgstr ""
"Project-Id-Version: plasma-applet_org.kde.ktp-contactlist\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2015-03-17 11:42+0000\n"
"PO-Revision-Date: 2015-03-17 21:50-0300\n"
"Last-Translator: André Marcelo Alvarenga <alvarenga@kde.org>\n"
"Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"

#: org.kde.ktp-contactlist/contents/ui/ContactList.qml:56
msgid "Go online"
msgstr "Conectar-se"

#: org.kde.ktp-contactlist/contents/ui/ContactList.qml:73
msgid "Search contacts..."
msgstr "Procurar contatos..."

#: org.kde.ktp-contactlist/contents/ui/ExternalLabel.qml:37
msgid "ContactList"
msgstr "Lista de contatos"

#: org.kde.ktp-contactlist/contents/ui/main.qml:68
msgctxt "Means 'Connecting your IM accounts', it's in the applet tooltip"
msgid "Connecting..."
msgstr "Conectando..."

#: org.kde.ktp-contactlist/contents/ui/main.qml:71
#, kde-format
msgctxt ""
"The arg is the presence name (as is in ktp-common-internals.po, eg. Changing "
"Presence to Away..., it's in the applet tooltip"
msgid "Changing Presence to %1..."
msgstr "Mudar a presença para %1..."

#: org.kde.ktp-contactlist/contents/ui/main.qml:91
msgid "Instant Messaging Settings..."
msgstr "Configurações das mensagens instantâneas..."

#: org.kde.ktp-contactlist/contents/ui/main.qml:92
msgid "Contact List..."
msgstr "Lista de contatos..."

#: org.kde.ktp-contactlist/contents/ui/main.qml:95
msgid "Add New Contacts..."
msgstr "Adicionar novos contatos..."

#: org.kde.ktp-contactlist/contents/ui/main.qml:96
msgid "Join Chat Room..."
msgstr "Entrar na sala de bate-papo..."

#: org.kde.ktp-contactlist/contents/ui/main.qml:99
msgid "Make a Call..."
msgstr "Fazer uma chamada..."

#: org.kde.ktp-contactlist/contents/ui/main.qml:102
msgid "Send a File..."
msgstr "Enviar um arquivo..."